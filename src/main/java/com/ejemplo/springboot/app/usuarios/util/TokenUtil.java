package com.ejemplo.springboot.app.usuarios.util;

import java.io.UnsupportedEncodingException;

import org.apache.commons.codec.binary.Base64;

import com.ejemplo.springboot.app.usuarios.oauth.DecodedToken;
import com.google.gson.Gson;

public final class TokenUtil {

	public static boolean getDecoded(String encodedToken, String username) throws UnsupportedEncodingException {
		encodedToken = encodedToken.replace("Bearer ", "");
	    String[] pieces = encodedToken.split("\\.");
	    String b64payload = pieces[1];
	    String jsonString = new String(Base64.decodeBase64(b64payload), "UTF-8");
	    boolean authorizedUser = false;
	    DecodedToken decode = new Gson().fromJson(jsonString, DecodedToken.class);
	    if(decode.getUser_name().equals(username) ) {
	    	authorizedUser = true;
	    }
	    
	    return authorizedUser;
	}
}
