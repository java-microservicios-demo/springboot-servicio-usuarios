package com.ejemplo.springboot.app.usuarios.models.response;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

public class UsuarioResponse {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long id;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String username;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Boolean enabled;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String nombre;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String apellido;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private List<NotaResponse> notas;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Boolean getEnabled() {
		return enabled;
	}
	public void setEnabled(Boolean enabled) {
		this.enabled = enabled;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public List<NotaResponse> getNotas() {
		return notas;
	}
	public void setNotas(List<NotaResponse> notas) {
		this.notas = notas;
	}
}
