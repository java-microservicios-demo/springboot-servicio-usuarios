package com.ejemplo.springboot.app.usuarios.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ejemplo.springboot.app.usuarios.models.dao.NotaDao;
import com.ejemplo.springboot.app.usuarios.models.entity.Nota;
import com.ejemplo.springboot.app.usuarios.models.response.NotaResponse;
import com.ejemplo.springboot.app.usuarios.services.NotaService;
import com.ejemplo.springboot.app.usuarios.util.MapperUtil;

@Service
public class NotaServiceImpl implements NotaService {

	@Autowired
	private NotaDao notaDao;
	
	@Override
	public NotaResponse guardarNotaUsuario(Nota nota) {
		Nota not = notaDao.save(nota);
		String jsonObj = MapperUtil.objectToJson(not);
		NotaResponse userObj = MapperUtil.fromJson(jsonObj, NotaResponse.class);
		return userObj;
	}
}
