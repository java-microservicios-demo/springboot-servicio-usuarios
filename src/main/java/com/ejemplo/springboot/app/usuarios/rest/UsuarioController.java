package com.ejemplo.springboot.app.usuarios.rest;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.ejemplo.springboot.app.usuarios.models.response.UsuarioResponse;
import com.ejemplo.springboot.app.usuarios.services.UsuarioService;
import com.ejemplo.springboot.app.usuarios.util.TokenUtil;

@RestController
public class UsuarioController {

	@Autowired
	private UsuarioService usuarioService;
	
	@GetMapping("/usuarios/notas")
	public ResponseEntity<UsuarioResponse> obtenerUsuarioNotas(@RequestHeader("Authorization") String token, @Param("username") String username) throws UnsupportedEncodingException{
		if(TokenUtil.getDecoded(token, username)) {
			UsuarioResponse usuario = usuarioService.findByUsername(username);
			return new ResponseEntity<>(usuario, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
		}
	}
}
