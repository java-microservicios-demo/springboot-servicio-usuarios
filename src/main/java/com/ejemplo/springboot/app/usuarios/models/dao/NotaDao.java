package com.ejemplo.springboot.app.usuarios.models.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ejemplo.springboot.app.usuarios.models.entity.Nota;

public interface NotaDao extends JpaRepository<Nota, Long> {

}
