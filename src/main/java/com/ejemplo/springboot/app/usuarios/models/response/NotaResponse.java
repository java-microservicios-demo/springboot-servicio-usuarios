package com.ejemplo.springboot.app.usuarios.models.response;

import com.fasterxml.jackson.annotation.JsonInclude;

public class NotaResponse {

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private Long id;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String curso;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String nota;
	@JsonInclude(JsonInclude.Include.NON_NULL)
	private UsuarioResponse usuario;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	public String getNota() {
		return nota;
	}
	public void setNota(String nota) {
		this.nota = nota;
	}
	public UsuarioResponse getUsuario() {
		return usuario;
	}
	public void setUsuario(UsuarioResponse usuario) {
		this.usuario = usuario;
	}
}
