package com.ejemplo.springboot.app.usuarios.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.ejemplo.springboot.app.usuarios.models.entity.Nota;
import com.ejemplo.springboot.app.usuarios.models.response.NotaResponse;
import com.ejemplo.springboot.app.usuarios.services.NotaService;

@RestController
public class NotaController {

	@Autowired
	private NotaService notaService;
	
	@PostMapping("/notas/usuario")
	@ResponseStatus(HttpStatus.CREATED)
	public NotaResponse crearNota(@RequestBody Nota nota) {
		return notaService.guardarNotaUsuario(nota);
	}
}
