package com.ejemplo.springboot.app.usuarios.oauth;

import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class DecodedToken {

	private String exp;
	private String user_name;
	private List<String> authorities;


	public String toString() {
	    Gson gson = new GsonBuilder().setPrettyPrinting().create();
	    return gson.toJson(this);
	}

	public String getExp() {
		return exp;
	}

	public void setExp(String exp) {
		this.exp = exp;
	}

	public String getUser_name() {
		return user_name;
	}

	public void setUser_name(String user_name) {
		this.user_name = user_name;
	}

	public List<String> getAuthorities() {
		return authorities;
	}

	public void setAuthorities(List<String> authorities) {
		this.authorities = authorities;
	}
}
