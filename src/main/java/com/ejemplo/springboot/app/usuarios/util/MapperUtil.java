package com.ejemplo.springboot.app.usuarios.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;

public final class MapperUtil {

	private MapperUtil() {}
    private static final Gson GSON = new Gson();
    public static String toJson(Object obj) {
        return GSON.toJson(obj);
    }
    
    public static <T> T fromJson(String jsonObj, Class<T> type) {
    	return GSON.fromJson(jsonObj, type);
    }
    
    public static String objectToJson(Object obj) {

    	try {
    		return new ObjectMapper().writeValueAsString(obj);
    	} catch (JsonProcessingException ex) {

 	    }
 	   	return null;
    }
}
